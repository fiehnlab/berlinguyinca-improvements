package edu.uci.ics.crawler4j.crawler;

public class AuthInfo
{
    public enum AuthenticationType {
        BASIC_AUTHENTICATION, FORM_AUTHENTICATION
    }
    
    public enum HttpMethod {
	GET, POST
    }
    
    private AuthenticationType authenticationType;
    
    private String username;
    
    private String password;
    
    private String host;
    
    private String loginTarget;
    
    private HttpMethod httpMethod;
    
    private String scheme;
    
    private int port;

    /**
     * @return the authenticationType
     */
    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }

    /**
     * @param authenticationType the authenticationType to set
     */
    public void setAuthenticationType(AuthenticationType authenticationType) {
        this.authenticationType = authenticationType;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the loginTarget
     */
    public String getLoginTarget() {
        return loginTarget;
    }

    /**
     * @param loginTarget the loginTarget to set
     */
    public void setLoginTarget(String loginTarget) {
        this.loginTarget = loginTarget;
    }

    /**
     * @return the httpMethod
     */
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    /**
     * @param httpMethod the httpMethod to set
     */
    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }
}
